#!/usr/bin/env python3
import requests
import sys

def dehash(variable_list):

    inArg = variable_list[0]
    outdir = variable_list[4]
    apiuser = variable_list[7]
    apikey = variable_list[8]

    if apikey == "" or apiuser == "":
        print("[-]   No Dehashed API key and/or credenials, returning to test progression")
        return

    outfile = outdir + inArg + "-breach.csv"
    url = "https://api.dehashed.com/search?size=10000&query=email:"

    resp = requests.get(url+inArg,headers={"Accept": "application/json","User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"},auth=(apiuser,apikey))

    if resp.status_code == 200:
        JSONData = resp.json()
    else:
        print("API Error: {}".format(resp.status_code),file=sys.stderr)
        print("Full Response:",file=sys.stderr)
        print(resp.headers,file=sys.stderr)
        print("",file=sys.stderr)
        print(resp.text,file=sys.stderr)
        exit()

    entries = JSONData["entries"]
    if entries and len(entries):
        headers = list(entries[0].keys())
        headers.remove("id")
        outbody = (','.join(['"'+header+'"' for header in headers]) + "\n")
        for entry in entries:
            additional = (','.join(['"'+entry[header].replace('"','\\"')+'"' if entry[header] else '""' for header in headers]) + "\n")
            print(additional)
            outbody = outbody + additional

        print("[+]   Writing results to {}".format(outfile))
        with open(outfile, 'w') as out_file:
            out_file.write(outbody)
        
    else:
        print("No results found.",file=sys.stderr)
