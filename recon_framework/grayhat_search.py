#!/bin/python

import requests

def grayhat_search(variable_list):

    targetName = variable_list[2]
    grayhat = variable_list[5]

    if grayhat == "":
        print("[-]   No Grayhat API key, returning to test progression")
        return

    url = "https://buckets.grayhatwarfare.com/api/v1/files/{}/0/1000?stopextensions=png,jpg&access_token={}".format(targetName, grayhat)
    data = requests.get(url).json()['files']

    for x in data:
        print(x['url'])