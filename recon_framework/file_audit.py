#!/usr/bin/env python3

from netaddr import *
from dns_chomper import url_convert

def url_check(candidate):
    print("[+]   Attempting to resolve \"{}\" to IP address".format(candidate.strip()))
    success = url_convert(candidate.strip())
    if success != None:
        return candidate
    else:
        return False

def verifyIP(ip):
    if ip.count(".") != 3:
        return False
    else:
        for octet in ip.split("."):
            if int(octet) < 0 or int(octet) > 255:
                return False
    return True

def parseIP(addr):
    wCnt = addr.count("/")
    tCnt = addr.count("-")
    if (wCnt + tCnt) > 1:
        return False
    elif wCnt == 1:
        ip, sub = addr.split("/")
        if verifyIP(ip) and int(sub) >= 0 and int(sub) <= 32:
            return addr
        else:
            return False
    elif tCnt == 1:
        ip1, ip2 = addr.split("-")
        if ip2.count(".") == 0:
            ip2 = ip1[:ip1.rfind(".")+1] + ip2
        if verifyIP(ip1) and verifyIP(ip2):
            return IPRange(ip1,ip2)
        else:
            return False
    else:
        if verifyIP(addr):
            return addr
        else:
            possible_url_ip = url_check(addr)
            if possible_url_ip:
                return possible_url_ip
            else:
                return False

def file_audit(targetDomain,scope,outdir):

    scopeList = IPSet()
    URLlist = []
    outfile = outdir + targetDomain + "-verified_scope"

    i = 0
    with open(scope,"r") as f:
        for line in f.read().splitlines():
            i += 1
            if line != "":
                address = parseIP(line)
                if address:
                    try:
                        scopeList.add(address)
                    except:
                        URLlist.append(address)
                else:
                    print("[-]   Invalid scope address line: [{}] {}".format(i,line))
    print("[+]   Scope size: {:,} targets".format(len(scopeList)+len(URLlist)))

    print("[+]   Writing verified scope list to {}".format(outfile))
    with open(outfile, 'w') as out_file:
        out_file.write("\n".join(str(ip) for ip in scopeList))
        out_file.write("\n")
        out_file.write("\n".join(str(ip) for ip in URLlist))

    return outfile