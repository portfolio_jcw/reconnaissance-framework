#!/bin/python3

from pythonping import ping
import re

# Jess wrote this!

# open scope file #iterate through IPs

def baby_ping(variable_list):

    targetDomain = variable_list[0]
    scopePath = variable_list[3]
    outdir = variable_list[4]

    if scopePath == None:
        print("[-]   No scope file")
        return

    print("[-]   WARNING WARNING Pings will fail if the module is not run as root\n")

    ping_returns="IPs that respond to ping requests:\n"
    icmp_returns=outdir + targetDomain + "-icmp_returns"

    with open (scopePath, "r") as ips:
        for line in ips:
            try:
                result=ping(line.strip(), count=1, timeout=2)
                if result.packets_lost == 0:
                    print('[+]   {} responds to ping requests'.format(line.strip()))
                    ping_returns=ping_returns+"\n"+line.strip()
                else:
                    print('[-]   {} failed'.format(line.strip()))
            except:
                print('[-]   {} failed'.format(line.strip()))

    #output which ones respond to ping request
    with open(icmp_returns, 'w') as out_file:
        out_file.write(ping_returns)

    print("\n[+]   Writing ping report to {}\n".format(icmp_returns))
    print(ping_returns)