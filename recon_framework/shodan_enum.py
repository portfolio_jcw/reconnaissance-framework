#!/bin/python3

import shodan

def shodan_enum(variable_list):

    looker = ['ip_str',
              'ports',
              'product',
              'version',
              'hostnames',
              'cpe']

    targetDomain = variable_list[0]
    outdir = variable_list[4]
    SHODAN_API = variable_list[6]
    if SHODAN_API == "":
        print("[-]   No Shodan API key, returning to test progression")
        return
    api = shodan.Shodan(SHODAN_API)
    scope = outdir + targetDomain + "-verified_scope"
    outfile = outdir + targetDomain + "-shodan_data"

    scope_object = []
    try:
        with open (scope, "r") as targets:
            for target in targets:
                scope_object.append(target.strip())
    except:
        print("[-]    Error reading verified scope file, returning to test progression")
        return
    
    print("IP/URL\tPorts\tProduct\tVersion\tHostnames\tcpe")

    # Shodan API maxes out at 100 IPs in call, so break scope into 100 IP chunks

    report = []

    chunks = [scope_object[x:x+100] for x in range(0, len(scope_object), 100)]
    for chunk in chunks:
        try:
            data = api.host(chunk)
            if type(data) == list:
                for host in data:
                    report.append(host)
                    host_data = {k:v for e in host['data'] for (k,v) in e.items()}
                    for look in looker:
                        try:
                            outer = host[look]
                            if type(outer) == list:
                                outer = ('; '.join(map(str,outer)))
                        except:
                            try:
                                outer = host_data[look]
                                if type(outer) == list:
                                    outer = ('; '.join(map(str,outer)))
                            except:
                                outer = "N/A"

                        print(outer, end = '\t')
                    print("")
            elif type(data) == dict:
                report.append(data)
                host_data = {k:v for e in data['data'] for (k,v) in e.items()}
                for look in looker:
                    try:
                        outer = data[look]
                        if type(outer) == list:
                            outer = ('; '.join(map(str,outer)))
                    except:
                        try:
                            outer = host_data[look]
                            if type(outer) == list:
                                outer = ('; '.join(map(str,outer)))
                        except:
                            outer = "N/A"

                    print(outer, end = '\t')
                print("")

        except shodan.APIError as e:
            print("[-]   Shodan API error: {}".format(e))
            
    print("\n[+]   Writing aggregate Shodan data to {}".format(outfile))
    with open(outfile, 'w') as out_file:
        out_file.write(str(report))
