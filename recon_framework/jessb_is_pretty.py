#!/bin/python3

def jessb_is_pretty(variable_list):
    txt = input("\nWhat is your first name?")
    name = str(txt)

    print ("\nHello, " + name + "! You are pretty and wonderful! Here is a recipe for cookies:")
    print ("Ingredients:\n1 1/2 cups of creamy peanut butter\n1 1/4 cups of white sugar\n2 teaspoons vanilla extract\n1 egg, beaten")
    print ("Instructions:\nPreheat oven to 350°.\nMix peanut butter and sugar.\nAdd vanilla and egg.\nMake cookie dough balls.\nPress criss-cross pattern with fork.\nBake 10-12 minutes.")

