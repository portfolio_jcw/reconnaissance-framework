#!/bin/python3

import json
import dns.zone
import dns.resolver
import time
import readchar
import os

from url_walker import url_walker

def maybe_skip_this(dedup_domlist,timer,outdir,targetDomain):
    print("\n<enter> to continue or <s>kip operation ", end = '')
    cntinue = readchar.readkey()
    if cntinue == "s":
        return
    else:
        whichtest = "subdomainswalk-"
        print("\n")
        url_walker(whichtest,dedup_domlist,timer,outdir,targetDomain)

def generic_try(friendly):
    server_list = []
    try:
        answer = dns.resolver.resolve(friendly, 'CNAME')
        for server in answer:
            attempt = "CNAME {}".format((server.to_text()).rstrip("."))
        print("{}|{}".format(friendly,attempt))
    
    except:
        try:
            answer = dns.resolver.resolve(friendly, 'A')
            for server in answer:
                server_list.append(server.to_text())
            attempt = server_list
            print("{}|{}".format(friendly,(','.join(attempt))))
        except:
            attempt = "N/A"
            print("{}|{}".format(friendly,attempt))
    return attempt

def sub_digester(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]
    scopePath = variable_list[3]
    outdir = variable_list[4]
    scope_switch = False

    print("\nLimit output to scope IPs? <y/n> ", end = '')
    cntinue = readchar.readkey()
    if cntinue == "y":
        if scopePath != None:
            with open ((outdir+targetDomain+"-verified_scope"), "r") as targets:
                target_set = []
                for target in targets:
                    target_set.append(target.strip())
            scope_switch = True
            carveout = []
            carveout_list = []
        else:
            print("[-]   No scope file to injest")

    domlist = []
    infile = outdir + targetDomain + "-harv.json"
    if os.path.isfile(infile) == False:
        print("\n[-]    Must run theHarvester first!")
        return
    data = json.load(open(infile))

    for host in data["hosts"]:
        domlist.append(host.split(":")[0])

    dedup_domlist = sorted([*set(domlist)])

    print("\n[+]   Verfied, formatted subdomains\n")

    reportout = []
    subdom_list = {}
    for dom in dedup_domlist:
        attempt = generic_try(dom)
        subdom_list[dom] = attempt
        if type(attempt) != str and scope_switch == True:
            for ip in attempt:
                if ip in target_set:
                    carveout.append("{}|{}".format(dom,(','.join(attempt))))
                    carveout_list.append(dom)
                    break
        if type(attempt) != str:
            reportout.append("{}|{}".format(dom,(','.join(attempt))))
        else:
            reportout.append("{}|{}".format(dom,attempt))
        time.sleep(timer)

    outfile = outdir + targetDomain + "-subdomains.txt"
    print("\n[+]   Writing results to {}".format(outfile))
    with open(outfile, 'w') as out_file:
        for sub in reportout:
            out_file.write(sub + "\n")

    if scope_switch == True:
        print("\n[+]   Scope-constrained subdomain list\n")
        for sub in carveout:
            print(sub)
        dedup_domlist = carveout_list
        outfile = outdir + targetDomain + "-scope-subdomains.txt"
        print("\n[+]   Writing results to {}".format(outfile))
        with open(outfile, 'w') as out_file:
            for sub in carveout:
                out_file.write(sub + "\n")

    print("\n\n[+]  Subdomain sites verification (ports 80, 443)")
    maybe_skip_this(dedup_domlist,timer,outdir,targetDomain)



