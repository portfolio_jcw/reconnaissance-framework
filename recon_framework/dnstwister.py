#!/bin/python3

import dnstwist
import whois
import time
import readchar
from ipwhois import IPWhois
from url_walker import url_walker

def maybe_skip_this(whichtest,url_list,timer,outdir,targetDomain):
    print("\n<enter> to continue or <s>kip operation ", end = '')
    cntinue = readchar.readkey()
    if cntinue == "s":
        return
    else:
        print("\n")
        # url_walker(whichtest,target_set,timer,outdir,targetDomain)
        url_walker(whichtest,url_list,timer,outdir,targetDomain)

def call_whois(variable_list):
    targetDomain = variable_list[0]
    data = whois.whois(targetDomain)
    for datum in data:
        try:
            if type(data[datum]) == list:
                content = (', '.join(data[datum]))
                print("{}: {}".format(datum,content))    
            else:
                print("{}: {}".format(datum,data[datum]))
        except:
            pass

def call_arin(variable_list):
    targetDomain = variable_list[0]
    timer = variable_list[1]
    outDir = variable_list[4]
    host_list = []

    looker = ['handle',
            'start_address',
            'end_address',
            'cidr',
            'ip_version',
            'type',
            'name',
            'country',
            'parent_handle'
    ]

    try:
        with open ((outDir + targetDomain + "-verified_scope"), "r") as scope:
            for line in scope:
                host_list.append(line)
    except:
        print("[-]   Error reading scope file, returning to test progression")
        return

    for host in host_list:
        print("\n[+]   {}".format(host))
        try:
            data = IPWhois(host.strip())
            lookup = data.lookup_rdap()
            for look in looker:
                try:
                    print("{}: {}".format(look,(lookup['network'])[look]))
                except:
                    print("{}: N/A".format(look))

            entities = (lookup['entities'])

            for ent in entities:

                contact = ((lookup['objects'])[ent])['contact']
                try:
                    print("name: ",contact['name'])
                except:
                    print("name: N/A")
                try:
                    print("kind: ",contact['kind'])
                except:
                    print("kind: N/A")
                try:  
                    print("address: ",(((contact['address'])[0])['value']).replace('\n',', '))
                except:
                    print("address: N/A")
                try:
                    print("email: ",(((contact)['email'])[0])['value'])
                except:
                    print("email: N/A")

            time.sleep(timer)

        except:
            print("[-]   No data")

def dnstwister(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]
    outdir = variable_list[4]
    data = dnstwist.run(domain = targetDomain, registered = True, format = 'null')
    url_list = []

    for record in data:
        url = record['domain']
        url_special = '.'.join(((record['domain']).split('.'))[-2:])
        url_list.append(url)
        registrar = (whois.whois(url_special)).registrar

        print("{}|{}".format(url,registrar))
        time.sleep(timer)

    print("\n\n[+]  Registered potentially malicious URL verification in browser")
    #url_walker(whichtest,target_set,timer,outdir,targetDomain)
    whichtest = "squatting-"
    maybe_skip_this(whichtest,url_list,timer,outdir,targetDomain)