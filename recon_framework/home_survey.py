#!/bin/python3

import time
import requests

def robots(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]

    attempts = [
    'https://{}/robots.txt'.format(targetDomain),
    'https://www.{}/robots.txt'.format(targetDomain),
    'http://{}/robots.txt'.format(targetDomain),
    'http://www.{}/robots.txt'.format(targetDomain)
    ]

    for attempt in attempts:
        try:
            robot = requests.get(attempt, verify=False, timeout=5)
            if robot.status_code == 200:
                print("\n   [+] {}\n".format(attempt))
                print(robot.text)
                break
            else:
                print("   [-] {}".format(attempt))
        except:
            print("[-]    [{}] Connection error".format(attempt))
        time.sleep(timer)

def sitemap(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]

    attempts = [
        '/sitemap.xml',
        '/sitemap_index.xml',
        '/sitemap-index.xml',
        '/sitemap/',
        '/post-sitemap.xml',
        '/sitemap/sitemap.xml',
        '/sitemap/index.xml',
        '/rss/',
        '/rss.xml',
        '/sitemapindex.xml',
        '/sitemap.xml.gz',
        '/sitemap_index.xml.gz',
        '/sitemap.php',
        '/sitemap.txt',
        '/atom.xml'
    ]

    for attempt in attempts:
        for protocol in ['https://www.','https://']:
            url = protocol + targetDomain + attempt
            try:
                sitem = requests.get(url, verify=False, timeout=5)
                if sitem.status_code == 200:
                    print("\n   [+] {}\n".format(url))
                    print(sitem.text)
                else:
                    print("   [-] {}".format(url))
            except:
                print("[-]    [{}] Connection error".format(attempt))
            time.sleep(timer)
