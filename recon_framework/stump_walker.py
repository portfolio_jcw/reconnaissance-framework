#!/bin/python3

# stump url walker

from url_walker import url_walker

import sys

whichtest = "stump-"
outdir = "./"
targetDomain = "texasheartmedical.org"
out = open(sys.argv[1],'r')
target_set = out.readlines()
timer = 10

url_walker(whichtest,target_set,timer,outdir,targetDomain)