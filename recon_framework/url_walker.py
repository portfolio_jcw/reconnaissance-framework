#!/bin/python

import webbrowser
import time
import readchar

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import base64

b = webbrowser.get('firefox')

def url_walker(whichtest,target_set,timer,outdir,targetDomain):

    print("\nBuild a screen<g>rab file, <m>anually visit web pages, do <b>oth, or <s>kip? ", end = '')
    cntinue = readchar.readkey()
    if cntinue == "g" or cntinue == "b":

        print("\n\n[+]   Note that conservative throttling (2+ seconds) will increase the probablitity of a fully-loaded screenshot.")

        counter = 0
        screengrabs = ""
        timeout = timer # the amount of time we spend trying to reach a site
        maxtabs = 20 # how many active tabs can selenium have open?  I'm not sure...
        print("")

        # create a web driver instance
        options = Options()
        options.add_argument("-headless")
        driver = webdriver.Firefox(options=options)
        driver.set_page_load_timeout(10)

        for url in target_set:
            
            print("[+]   {}".format(url.strip()))

            for proto in ["http", "https"]: 

                    try:
                        initial_url = proto + "://" + url.strip()
                        driver.get(initial_url)
                        saveplace = "/tmp/" + whichtest + proto + "-" + url.strip() + ".png"
                        final_url = (str(driver.current_url))
                        time.sleep(timer)
                        driver.save_screenshot(saveplace)
                        print("[+]    {} screenshot [final redirect destination {}] saved at {}".format(proto,final_url,saveplace))
                        with open(saveplace, 'rb') as image_file:
                            bytes_encoded = base64.b64encode(image_file.read())
                            encoded_string = bytes_encoded.decode("ascii")
                        screengrabs = screengrabs + "<h1>" + initial_url + "</h1><h2>final redirect destination " + final_url + "</h2><img src=\"data:image/png;base64," + encoded_string + "\" />"
                        #time.sleep(2)
                    except:
                        print("[-]    {} Page load error.".format(proto))
            
            counter +=1
            if counter == maxtabs:
                driver.quit()
                driver = webdriver.Firefox(options=options)
                driver.set_page_load_timeout(timeout) # timeout in case we can't reach a site
                counter = 0

        driver.quit()
        screengrabs_save = outdir + whichtest + targetDomain + "-screenshots.html"
        print("\n[+]   Writing screenshots file to {}".format(screengrabs_save))
        with open(screengrabs_save, 'w') as out_file:
                out_file.write(screengrabs)

        b.open("file://" + screengrabs_save)

    # do you want to manually review web pages?
        
    if cntinue == "m" or cntinue == "b":

        counter = 0
        cntinue = ""
        print("")

        for url in target_set:
            if cntinue == "a":
                break
            print("[+]   {}".format(url.strip()))
            b.open("http://{}".format(url), new=2)
            time.sleep(timer)
            b.open("https://{}".format(url), new=2)
            time.sleep(timer)
            counter +=1
            if counter == 5:
                print("<enter> to continue or <a>bort to return to test progression")
                cntinue = readchar.readkey()
                counter = 0

def google_walker(sites,targetDomain,timer,targetName):

    counter = 0
    cntinue = ""

    for site in sites:
        if cntinue == "a":
            break
        print("[+]   {}".format(site))
        b.open("https://google.com/search?q=site:" + site + "+%22" + targetDomain + "%22")
        time.sleep(timer)
        b.open("https://google.com/search?q=site:" + site + "+%22" + targetName + "%22")
        time.sleep(timer)
        counter +=1
        if counter == 5:
            print("<enter> to continue or <a>bort to return to test progression")
            cntinue = readchar.readkey()
            counter = 0

def file_walker(filetypes,targetDomain,timer,targetName):

    counter = 0
    cntinue = ""

    for type in filetypes:
        if cntinue == "a":
            break
        print("[+]   {}".format(type))
        b.open("https://google.com/search?q=filetype:" + type + "+site:" + targetDomain)
        time.sleep(timer)
        b.open("https://google.com/search?q=filetype:" + type + "+%22" + targetName + "%22")
        time.sleep(timer)
        counter +=1
        if counter == 5:
            print("<enter> to continue or <a>bort to return to test progression")
            cntinue = readchar.readkey()
            counter = 0

def ip_walker(variable_list):

    targetDomain = variable_list[0]
    scopePath = variable_list[3]
    outdir = variable_list[4]
    timer = variable_list[1]

    if scopePath == None:
        print("[-]   No scope file")
        return

    whichtest = "ipwalk-"

    with open ((outdir + targetDomain + "-verified_scope"), "r") as targets:
        target_set = []
        for target in targets:
            target_set.append(target)
        url_walker(whichtest,target_set,timer,outdir,targetDomain)

