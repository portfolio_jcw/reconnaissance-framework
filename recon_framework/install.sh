#/bin/bash

# Install python dependencies

# Please note that the ping module will not run without root privs, so either run everythng as root or reconcile yourself to not being able to use that part of the script.

# usage ./install.sh <directory hosting the Python virtual environment>

pythonVenv=$1

if [ -z $pythonVenv ]
then
    echo "\n[-] Please supply a fully qualified directory path for the creation of a Python virtual environment.  For example:"
    echo "\n./install.sh /home/kali/python_venv_1"
    exit
fi

echo "\nGeneral Recon Framework Installation"
echo "\n[+] Creating Python virtual environment to host dependencies at $pythonVenv"

python3 -m venv $pythonVenv

echo "[+] Installing Python libraries"

$pythonVenv/bin/pip install python-whois readchar pythonping dnstwist shodan ipwhois netaddr selenium geckodriver bs4

echo "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"

echo "\nTo access the application, type:"
echo "\n$pythonVenv/bin/python3 <application directory>/framework.py"
echo "\nor consider adding the following alias line to you .zshrc or .bashrc file:"
echo "\nframework='$pythonVenv/bin/python3 <application directory>/framework.py'"
