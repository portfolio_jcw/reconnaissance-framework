#!/bin/python3

import os

def call_theHarvester(variable_list):

    targetDomain = variable_list[0]
    outdir = variable_list[4]

    try:
        os.system("theHarvester -d " + targetDomain + " -b all -f " + outdir + targetDomain + "-harv.json" + " -b anubis,baidu,bevigil,binaryedge,bing,bingapi,bufferoverun,censys,certspotter,criminalip,crtsh,dnsdumpster,duckduckgo,fullhunt,github-code,hackertarget,hunter,hunterhow,intelx,netlas,onyphe,otx,pentesttools,projectdiscovery,rapiddns,rocketreach,securityTrails,sitedossier,subdomaincenter,subdomainfinderc99,threatminer,tomba,urlscan,virustotal,yahoo,zoomeye")
    except:
        print("[-]   Error calling theHarvester, please check that the application and dependencies are propery installed: https://github.com/laramies/theHarvester")

def call_pymeta(variable_list):

    targetDomain = variable_list[0]
    outdir = variable_list[4]
    timer = variable_list[1]

    reportFile = outdir + targetDomain + "-pymeta-recovered-files.csv"

    try:
        os.system("pymeta -d " + targetDomain + " -o " + outdir + " -s all -j " + str(timer) + " -f " + reportFile + " -m 100")
    except:
        print("[-]   Error calling pymeta, please check that the application and dependencies are properly installed: https://github.com/m8sec/pymeta")