#!/bin/bash

import webbrowser
import time
from url_walker import google_walker,file_walker

b = webbrowser.get('firefox')

paste_sites = [
    "pastebin.com",
    "drive.google.com",
    "docs.google.com",
    "sheets.google.com",
    "docs.microsoft.com",
    "wikileaks.org",
    "github.com"
]

social_sites = [
    "facebook.com",
    "linkedin.com",
    "youtube.com",
    "twitter.com",
    "pinterest.com",
    "tumblr.com",
    "instagram.com",
    "flickr.com",
    "vimeo.com",
    "brighttalk.com",
    "tiktok.com",

]

filetype_list = [
    "pdf",
    "xls",
    "xlsx",
    "doc",
    "docx",
    "txt",
    "log",
    "rtf",
    "csv",
    "bak",
    "php",
    "env"
]

def dorking(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]

    b.open("https://www.google.com/?q=filetype:bak%20inurl:%22htaccess\|passwd\|shadow\|htusers%22+site:" + targetDomain)
    time.sleep(timer)
    b.open("https://www.google.com/?q=intitle:%22index%20of%22+%22last%20modified%22+%22parent%20directory%22+site:" + targetDomain)
    time.sleep(timer)
    b.open("https://www.google.com/search?q=site:" + targetDomain + "+inurl:admin")
    time.sleep(timer)
    b.open("https://www.google.com/search?q=site:" + targetDomain + "+inurl:login")
    time.sleep(timer)
    b.open("https://www.google.com/search?q=site:*." + targetDomain + "+-www")
    time.sleep(timer)
    b.open("https://www.google.com/search?q=site:*.*." + targetDomain + "+-www")
    time.sleep(timer)
    b.open("https://www.google.com/search?q=site:" + targetDomain + "+%22choose+file%22")
    time.sleep(timer)
    b.open("https://www.google.com/search?q=site%3Aopenbugbounty.org+inurl%3Areports+intext%3A%22" + targetDomain + "%22")

def sites_recon(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]
    targetName = variable_list[2]

    google_walker(paste_sites,targetDomain,timer,targetName)

def social_recon(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]
    targetName = variable_list[2]

    google_walker(social_sites,targetDomain,timer,targetName)

    print("[+]   boardreader.com")
    b.open("https://boardreader.com/s/" + targetDomain + ".html;language=English")
    time.sleep(timer)
    b.open("https://boardreader.com/s/" + targetName + ".html;language=English")
    time.sleep(timer)

def filetype_recon(variable_list):

    targetDomain = variable_list[0]
    timer = variable_list[1]
    targetName = variable_list[2]

    file_walker(filetype_list,targetDomain,timer,targetName)

def ssl_audit(variable_list):

    targetDomain = variable_list[0]

    b.open("https://www.ssllabs.com/ssltest/analyze.html?d=" + targetDomain + "&hideResults=on&latest")