#!/bin/python3

#################

# Recon Framework

#################

from optparse import OptionParser
import sys
import readchar
import os.path
import os
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

from dnstwister import dnstwister,call_whois,call_arin
from dehash import dehash
from grayhat_search import grayhat_search
from dns_chomper import dns_chomper,domain_verify
from sub_digester import sub_digester
from call_bash_scripts import call_theHarvester #,call_pymeta
from dorking import dorking,sites_recon,social_recon,filetype_recon,ssl_audit
from home_survey import robots,sitemap
from baby_ping import baby_ping
from url_walker import ip_walker
from shodan_enum import shodan_enum
from file_audit import file_audit
from jessb_is_pretty import jessb_is_pretty

###################################################

version_number = "1.19"

###################################################

GRAYHAT_API = "358d038236af863a2e622933f8f86675"
SHODAN_API = "ZuBGVQxdeUi6WlbMsqR2sVg7Xf41oIjg"
DEHASHED_USER = "jward@frsecure.com"
DEHASHED_API = "459a9fad978d37aac3a9b01ff574a0af"

###################################################

# taking arguments

if ((len(sys.argv) < 3 or len(sys.argv) > 12) and '-h' not in sys.argv):
    print("\nGeneral Recon Framework Version: {}".format(version_number))
    print("\nUsage:")
    print("\npython3 %s -d <target domain> -n (optional search phrase) -t (optional throttle) -s (optional path to IP/URL scope) -a (optional autofire all tests) -o (optional output directory)\n" % sys.argv[0])
    print("Enclose complex strings in single quotes e.g. '/a/file/path' or 'Company Name'")
    print("Scope file should have one IP, URL, dashed range, or CIDR range per line")
    print("For ping module to work, script must be run as root\n")
    print("Example:\n")
    print("python3 %s -d behind-bars.com -n 'Behind Bars' -t 2 -s '/home/kali/scope.txt' -a -o '/tmp/engagement'\n" % sys.argv[0])
    sys.exit(1)

parser = OptionParser()
parser.add_option("-s", "--scope", help='Enter full path to engagement scope IP/URL list e.g. \"/home/kali/file.txt\"')
parser.add_option("-d", "--domain", help='Target domain e.g. \"behind-bars.com\" or \"gentex.com\"')
parser.add_option("-n", "--name", help='Target search phrase/name e.g. \"Behind Bars Bicycle Shop\" or \"Gentex\"')
parser.add_option("-t", "--throttle", help="Request throttling in seconds. 2.0 is standard, but defaults to 0 if no argument)")
parser.add_option("-a", "--autofire", help='Automatically runs all tests in sequence.  Does not open browser for site checks.',action="store_true")
parser.add_option("-o", "--outdir", help='Path to directory where output files will be written. Defaults to "/tmp/"')
(options, args) = parser.parse_args()

# argument processing, logging, and minimal dopey error checking

if options.outdir == None:
    print("[+]   Output directory path defaulting to /tmp/")
    outdir = "/tmp/"
else:
    try:
        os.mkdir(options.outdir)
        print("[+]   Creating {}".format(options.outdir))
    except:
        print("[+]   {} already exists".format(options.outdir))
    outdir = (options.outdir).rstrip('/') + "/"

if outdir[0] == '.':
    outdir = os.getenv('PWD') + outdir.lstrip('.')
    print("[+]   Requalifying directory as {}".format(outdir))

log_file = outdir + options.domain + "-recon"

class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open(log_file, 'a')
    def write(self, text):
            self.terminal.write(text)
            self.log.write(text)
    def flush(self):
            pass
        
sys.stdout = Logger()

print("[+]   Application version: {}".format(version_number))
print("[+]   Logging stdout to {}".format(log_file))

targetDomain = options.domain
if targetDomain == None:
    print("[-]   No domain specified")
    quit()
else:
    audit = domain_verify(targetDomain)
    if audit == None:
        quit()
targetName = options.name
if targetName == None:
    targetName = targetDomain
    print("[+]   Search string defaulting to '{}'".format(targetName))
if (options.throttle) != None:
    timer = int(options.throttle)
else:
    timer = 0
    print("[+]   Throttle defaulting to 0")
scopePath = options.scope
if (scopePath) != None and os.path.isfile(scopePath) == False:
    print("[-]   Scope file does not exist -- check path")
    quit()
elif (scopePath) != None:
    print("[+]   Verifying and processing scope file")
    scopePath = file_audit(targetDomain,scopePath,outdir)
autofire = options.autofire

variable_list = [targetDomain,timer,targetName,scopePath,outdir,GRAYHAT_API,SHODAN_API,DEHASHED_USER,DEHASHED_API]

# logic to skip a module, or autofiring

def maybe_skip_this(subroutine,variable_list,autofire):

    if autofire == True:
        print("\n")
        subroutine(variable_list)
        return
    print("\n<enter> to continue or <s>kip operation ", end = '')
    cntinue = readchar.readkey()
    if cntinue == "s":
        return
    else:
        print("\n")
        subroutine(variable_list)

# test modules

subroutines = [
    ["\n[+]   Domain whois record",call_whois],
    ["\n\n[+]   Scope ownership ARIN confirmation",call_arin],
    ["\n\n[+]   Domain robots.txt review",robots],
    ["\n\n[+]   Domain sitemap review",sitemap],
    ["\n\n[+]   Scope pingsweep",baby_ping],
    ["\n\n[+]   Scope IP sites observation (ports 80,443)",ip_walker],
    ["\n\n[+]   DNS records",dns_chomper],
    ["\n\n[+]   Domain SSL audit",ssl_audit],
    ["\n\n[+]   Fuzzing target domain for registered URLs (Brian, that means Domain Squatting)",dnstwister],
    ["\n\n[+]   Google dorking domain",dorking],
    ["\n\n[+]   Paste site enumeraton",sites_recon],
    ["\n\n[+]   Social media site enumeration",social_recon],
    ["\n\n[+]   Shodan enumeration",shodan_enum],
    ["\n\n[+]   Filetype enumeration",filetype_recon],
    ["\n\n[+]   Run theHarvester",call_theHarvester],
    ["\n\n[+]   Subdomain template processing",sub_digester],
    ["\n\n[+]   Checking public breach data",dehash],
    ["\n\n[+]   Open buckets keyword search",grayhat_search],
    ["\n\n[+]   Cookie Time!",jessb_is_pretty]   # Jess wrote this
]

# removed ["\n\n[+]   pymeta webscraper",call_pymeta], till I can figure out that app

# calling modules

for subroutine in subroutines:
    print(subroutine[0])
    maybe_skip_this(subroutine[1],variable_list,autofire)