# Reconnaissance Framework

A modular tool for acquiring OSINT information for use in penetration testing.

Written in Python.


To begin, run install.sh


General Recon Framework Version: 1.19

Usage:

python3 /home/kali/recon_tools/recon_framework/framework.py -d <target domain> -n (optional search phrase) -t (optional throttle) -s (optional path to IP/URL scope) -a (optional autofire all tests) -o (optional output directory)

Enclose complex strings in single quotes e.g. '/a/file/path' or 'Company Name'
Scope file should have one IP, URL, dashed range, or CIDR range per line
For ping module to work, script must be run as root

Example:

python3 /home/kali/recon_tools/recon_framework/framework.py -d behind-bars.com -n 'Behind Bars' -t 2 -s '/home/kali/scope.txt' -a -o '/tmp/engagement'

